" PLUGINS
so $HOME/.vim/plugins-config/gestor.config

" CONFIGURACIONES GENERALES
set title
set number    " Habilita los numero de linea
set mouse=a   " Permite interactuar con el mouse
" set nowrap    " No dividir la linea si es muy larga
set numberwidth=1   " Definir el ancho de los números de linea
set relativenumber    " Números de linea relativos
set clipboard=unnamed " Permite el uso de portapapeles en modo visual
syntax enable " Resaltado
set showcmd " Mostrar comandos escritos
set ruler " Ver la posición
set encoding=utf-8 " Cambiar la codificación de los archivos
set showmatch " Muestra el final del paréntesis seleccionado
set laststatus=2 " Barra de estatus siempre visible
set cursorline " Resalta la linea actual
set termguicolors  " Activa el true colors 
" set splitbelow  " Abrir ventanas abajo
" set termwinsize=15x0  " Cambiar tamaño de terminal (0=auto)
set background=dark

" Indentación de 2 espacios
set tabstop=2
set shiftwidth=2
set softtabstop=2
set shiftround
set expandtab   " Insertar espacios en lugar de <Tab>

" Búsqueda
set ignorecase    " Ignorar mayúsculas al hacer una búsqueda
set smartcase   " No ignorar mayúsculas si la palabra contiene mayúsculas

" Corrección Ortográfica 
set spell   " Activa la corrección ortográfica
set spelllang=en,es   " Corregir palabras usando diccionarios en español e ingles

" Mapping
let mapleader=","
nmap <Leader>s :set spell<CR>
nmap <Leader>S :set nospell<CR>
nmap <Leader>tt :tab :term<CR>
nmap <Leader>t :set sb<CR>:set nospr<CR>:set tws=15x0<CR>:term<CR>
nmap <Leader>T :set nosb<CR>:set nospr<CR>:set tws=0x65<CR>:vert:term<CR>
