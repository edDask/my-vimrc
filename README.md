# Vim Personalizado:
Vim con diversos plugins y configuraciones para mejorar la experiencia de desarrollo. Ocupa [vim-plug](https://github.com/junegunn/vim-plug) para instalar los plugins que se detallan a continuación.


# Plugins que Contiene:

## Instalados Automáticamente:
- [easymotion](https://github.com/easymotion/vim-easymotion)
- [nerdtree](https://github.com/preservim/nerdtree)
- [vim-tmux-navigator](https://github.com/christoomey/vim-tmux-navigator)
- [indentLine](https://github.com/Yggdroot/indentLine)
- [auto-pairs](https://github.com/jiangmiao/auto-pairs)
- [nerdcommenter](https://github.com/preservim/nerdcommenter)
- [rainbow](https://github.com/luochen1990/rainbow)
- [vim-airline](https://github.com/vim-airline/vim-airline)
- [vim-coloresque](https://github.com/gko/vim-coloresque)
- [vim-commentary](https://github.com/tpope/vim-commentary)
- [onedark.vim](https://github.com/joshdick/onedark.vim)

## Opcionales:
- coc.nvim
- tabular
- markdown-preview.nvim


# Requerimientos:

## Obligatorio
- git
- curl
- vim

## Plugins Opcionales:
- npm
- yarn
- node


# Instalación
- clonar el repo en ~/.vim
    ``` bash
    git clone <url> .
    ```
- En la raíz crear un link simbólico al archivo `.vimrc`
    ```
    ln -s .vim/.vimrc .
    ```
- Con eso ya se puede usar el comando `PluginInstall` dentro de vim
